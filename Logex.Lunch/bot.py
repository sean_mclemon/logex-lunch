import random
from slack_wrapper import SlackWrapper
from utils import is_list, today

days = [ "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday" ]
adjectives = [ "delightful", "wonderful", "splendid", "chilled out" ]

class Bot(object):
    def __init__(self, token, default_channel):
        self.slack = SlackWrapper(token)
        self.default_channel = default_channel
        self.commands = {}

    def add_command(self, name, func):
        self.commands[name] = func

    def handle_command(self, command):
        if command in self.commands.keys():
            result = self.commands[command]()
            if result:
                    return result
            else:
                return [ f"error executing the command '{command}'" ]
        else:
            restaurant_commands = [ r for r in self.commands.keys() if r != "all" ]
            random_restaurant = random.choice(restaurant_commands)
            other_restaurants = ", ".join([ r for r in restaurant_commands if r != "all" and r != random_restaurant ])
            return [ f"unable to understand the command, try '@{self.slack.bot_name} all' or '@{self.slack.bot_name} {random_restaurant}' (I also understand these: {other_restaurants})" ]

    def tick(self):
        command, channel = self.slack.next_command()
        if command:
            responses = self.handle_command(command)
            for response in responses:
                self.slack.send(response, channel)

    def announce(self):
        adjective = random.choice(adjectives)
        day = days[today()]
        message = f"hi everyone, it's a {adjective} {day} and your favourite lunch pal {self.slack.bot_name} is online! Please don't ask me any difficult questions ..."
        self.slack.send(message, self.default_channel)
