import datetime
from itertools import count, islice
import random

def random_emoji():
    return random.choice( [ 
        ":bento:",
        ":taco:",
        ":stew:",
        ":spaghetti:"
        ])

def strip_and_skip_blank_lines(lines):
    stripped_lines = [ line.strip() for line in lines ]
    return [ line for line in stripped_lines if line != "" ]

def nth(iterable, n, default=None):
    return next(islice(iterable, n, None), default)

def today():
    return datetime.datetime.today().weekday()

def is_list(o):
    return isinstance(o, list)