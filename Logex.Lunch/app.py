# Logex.Lunch
# Parses websites for nearby restaurants and posts their daily menu to the #Brno slack channel when you
# send it the string "do your thing"

import os
import time
from bot import Bot
from menu_parsers import *
from slackclient import SlackClient

RTM_READ_DELAY = 1

if __name__ == "__main__":
    #bot = Bot(os.environ.get('SLACK_BOT_TOKEN'))
    bot = Bot("xoxb-324701367478-ksndTRmp7sdOrv8t72IybHqO", "#brno_lunch")

    # setup the commands
    bot.add_command("tusto", tusto), 
    bot.add_command("padowetz", padowetz), 
    bot.add_command("mamut", mamut), 
    bot.add_command("annapurna", annapurna), 
    bot.add_command("piazza", piazza)
    bot.add_command("all", all_menus)

    # we're online, tell the world!
    #bot.announce()

    while True:
        bot.tick()
        time.sleep(RTM_READ_DELAY)