import requests
from bs4 import BeautifulSoup
from dailymenu import ItemisedMenu, LinkMenu
from pprint import pprint
from utils import strip_and_skip_blank_lines, nth, today

def all_menus():
    restaurants = [ tusto, padowetz, mamut, piazza, annapurna ]
    menus = []
    for restaurant in restaurants:
        menus += restaurant()
    return [ m for m in menus if m ]

# decorating a function with @menu_parser("http://example.com") will use requests
# to retrieve the page, parse it using BeautifulSoup pass this into the function
def menu_parser(url, encoding=None):                            
    def decorator(fn):                                            
        def decorated(*args,**kwargs):
            try:
                page_response = requests.get(url)
                if encoding:
                    page_response.encoding = encoding
                soup = BeautifulSoup(page_response.text, "html.parser")
                return [ str(fn(soup)) ]
            except:
                return [ None ]
        return decorated                                          
    return decorator       

@menu_parser("http://titanium.tusto.cz")
def tusto(soup):
    menu_section = soup.find(id="mcontent")

    raw_menu = strip_and_skip_blank_lines(menu_section.get_text().split("\n"))

    # once we've stripped out the blank lines from the menu section we're left with something like
    # nejaka polevka
    # 1. <first menu item>
    # 101 kc
    # 2. <second menu item>
    # 101 kc
    # ... etc
    menu_soup = raw_menu[0]
    menu_items = raw_menu[1::2]
    menu_prices = raw_menu[2::2]
    
    # ... so initialize the menu
    menu = [ menu_soup ]
    for i, menu_item in enumerate(menu_items):
        menu.append(f"{menu_item}: {menu_prices[i]}")

    return ItemisedMenu("Tusto", menu)

@menu_parser("http://restaurant-padowetz.cz/poledni-menu.htm", "utf-8")
def padowetz(soup):
    # padowetz shows every day on the page, and has them split into sections selectable by the user
    # the menus are tables under elements "t_pondeli", "t_utery", etc so we first need to select the 
    # correct of days
    days = [ "pondeli", "utery", "streda", "ctvrtek", "patek", "sobota", "nedele" ]
    today_selector = f"t_{days[today()]}"
    todays_menu = soup.find(id=today_selector)

    # after the "!!!! Seznam alergenů ..." string there's just a couple of beer options, so stop parsing
    menu_terminator = "!!!!"

    # each menu-item is a table row with the class "dvojklik", there are three table columns in each:
    #     col 0: portion weight
    #     col 1: description
    #     col 2: price
    menu_rows = todays_menu.find_all("tr", { "class": "dvojklik" })
    menu_elements = [ ]

    for menu_row in menu_rows:
        menu_cols = menu_row.find_all("td")
        description = menu_cols[1].get_text()
        price = menu_cols[2].get_text()

        if menu_terminator in description:
            break

        if len(price.strip()) > 0:
            menu_elements.append(f"{description}: {price}")
        else:
            menu_elements.append(f"{description}")

    menu = strip_and_skip_blank_lines(menu_elements)
    return ItemisedMenu("Padowetz", menu)

@menu_parser("http://www.mamut-pub.cz/tydenni-menu/")
def mamut(soup):

    # the structure of the menu on the mamut page:
    #     <div id="centerBack3">
    #     -> <strong> (title)
    #     -> <div> (we can ignore)
    #     -> <div>
    #        -> <div>
    #             (list of many <divs> containing menu items)
    menu_container = soup.find(id="centerBack3")
    
    # ... so grab the third child of this
    menu_divs = nth(menu_container.children, 3)
    
    # ... and extract all the divs underneath it
    raw_menu_divs = [ m.get_text() for m in menu_divs.find("div") ]

    menus = []
    current_menu = []

    for menu_line in raw_menu_divs:
        stripped_menu_line = menu_line.strip()
        
        # empty line => separator for each day, so add the current menu to the list and move on
        if not stripped_menu_line:
            menus.append(current_menu)
            current_menu = []
        else:
            current_menu.append(stripped_menu_line)

    return ItemisedMenu("Mamut", menus[today()])

# TODO: need to check if the URL for the "Tydeni Jidelnicek" changes. 
#           as-at 2018-03-04 it is "http://www.indicka-restaurace-annapurna.cz/index.php?option=com_content&view=article&id=2&Itemid=118"
#           as-at 2018-03-05 it is TBC
#       if it does not change (specifically itemid) then we can fetch it and parse the page to find the menu JPG
#       if not we're gonna have to do something funky (fetch main page and grab the menu url from the HTML, then parse *that*)
#def annapurna():
    #return LinkMenu("Annapurna", "http://www.indicka-restaurace-annapurna.cz/images/tydenijidelnicek/26022018.jpg")
@menu_parser("http://www.indicka-restaurace-annapurna.cz/index.php?option=com_content&view=article&id=2&Itemid=118")
def annapurna(soup):
    menu_img_link = soup.find("a", { "rel": "boxplus-tmeny" })["href"]
    return LinkMenu("Annapurna", f"http://www.indicka-restaurace-annapurna.cz/{menu_img_link}")

@menu_parser("http://www.piazza.cz/denni-menu.php", "utf-8")
def piazza(soup):
    #piazza has structure like 
    # <div class="pagecont">
    #    <h1>Denni menu</h1>
    #    <h3>Pondeli 26.02.2018</h3>
    #    <p>Smazak</p>
    #    <p>Tatarak</p>
    #    <h3>Utery 27.02.2018</h3>
    #    <p>Testoviny</p>
    #    ...

    menu_container = soup.find("div", { "class": "pagecont" })
    menus = []
    current_menu = []

    for i, menu_item in enumerate(menu_container.find_all( [ "h3", "p" ] )):
        if menu_item.name == "h3": 
            if i > 0:
                menus.append(current_menu)
                current_menu = []
        else:
            current_menu.append(menu_item.get_text())
    
    menus.append(current_menu)

    return ItemisedMenu("Piazza", menus[today()])