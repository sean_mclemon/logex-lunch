# menu stuff
class ItemisedMenu(object):
    def __init__(self, restaurant, menuItems):
        self.restaurant = restaurant
        self.menuItems = menuItems

    def __str__(self):
        menuStr = "\n".join( [f"• {m}" for m in self.menuItems ] ) 
        return f"*{self.restaurant}*:\n{menuStr}"

class LinkMenu(object):
    def __init__(self, restaurant, url):
        self.restaurant = restaurant
        self.url = url

    def __str__(self):
        return f"*{self.restaurant}*:\n<{self.url}|Click here!>"