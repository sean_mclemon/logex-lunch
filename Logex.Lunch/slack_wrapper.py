import re
from slackclient import SlackClient

mention_regex = "^<@(|[WU].+?)>(.*)"

class SlackWrapper(object):
    def __init__(self, token, icon_emoji=":bento:"):
        self.icon_emoji =icon_emoji
        self.slack_client = SlackClient(token)
        if self.slack_client.rtm_connect(with_team_state=False):
            auth_test = self.slack_client.api_call("auth.test")
            self.bot_name = auth_test["user"]
            self.bot_id = auth_test["user_id"]
            print(f"connected as @{self.bot_name} (id={self.bot_id})")
        else:
            raise Exception("Connection failed. Exception traceback printed above.")

    def next_command(self):
        slack_events = self.slack_client.rtm_read()
        for event in slack_events:
            if event["type"] == "message" and not "subtype" in event:
                user_id, message = self.parse_direct_mention(event["text"])
                if user_id == self.bot_id:
                    return message, event["channel"]
        return None, None

    def parse_direct_mention(self, message_text):
        matches = re.search(mention_regex, message_text)
        return (matches.group(1), matches.group(2).strip()) if matches else (None, None)

    def send(self, message, channel):
        self.slack_client.api_call(
            "chat.postMessage",
            channel=channel,
            text=message,
            icon_emoji=self.icon_emoji
        )
    